<?php

use App\Http\Controllers\Auth\SocialAuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\SiteMapController;
use Illuminate\Support\Facades\Route;

// Custom
use App\Http\Controllers\Custom\HomeFController;
use App\Http\Controllers\Custom\ArticleController;
use App\Http\Controllers\Custom\ProductController;
use App\Http\Controllers\Custom\APIs\HomeController as APIHomeController;
use App\Http\Controllers\Custom\APIs\ArticleController as APIArticleController;
use App\Http\Controllers\Custom\APIs\ProfileController as APIProfileController;
use App\Http\Controllers\Custom\APIs\ProductController as APIProductController;
use App\Http\Controllers\Custom\APIs\ProductCategoryController as APIProductCategoryController;
use App\Http\Controllers\Custom\APIs\ProductImageController as APIProductImageController;

// Custom
// Route::get('/admin/login', function() {
//     return redirect('https://admin.bioota.com');
// });
Route::get('/homef', [HomeFController::class, 'index']);
Route::get('/products', [ProductController::class, 'index']);
Route::get('/id/products', [ProductController::class, 'index']);

// Custom API
Route::get('/api/home', [APIHomeController::class, 'get']);
Route::get('/api/article', [APIArticleController::class, 'get']);
Route::get('/api/article/{id}', [APIArticleController::class, 'find']);
Route::get('/api/profile', [APIProfileController::class, 'get']);
Route::get('/api/product', [APIProductController::class, 'get']);
Route::get('/api/product_category', [APIProductCategoryController::class, 'get']);
Route::get('/api/product_image', [APIProductImageController::class, 'get']);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Language Route
Route::post('/lang', [LanguageController::class, 'index'])->middleware('LanguageSwitcher')->name('lang');
// For Language direct URL link
Route::get('/lang/{lang}', [LanguageController::class, 'change'])->middleware('LanguageSwitcher')->name('langChange');
Route::get('/locale/{lang}', [LanguageController::class, 'locale'])->middleware('LanguageSwitcher')->name('localeChange');
// .. End of Language Route

// Backend Routes
Route::get('/login', function () {
    return redirect('/');
});
Route::get('/register', function () {
    return redirect('/');
});

// Social Auth
Route::get('/oauth/{driver}', [SocialAuthController::class, 'redirectToProvider'])->name('social.oauth');
Route::get('/oauth/{driver}/callback', [SocialAuthController::class, 'handleProviderCallback'])->name('social.callback');

Route::Group(['prefix' => env('BACKEND_PATH')], function () {
    Auth::routes();
});
Route::get('/logout', function () {
    Auth::logout();
    return redirect('/');
})->name('logout');

// Start of Frontend Routes
// ../site map
Route::get('/sitemap.xml', [SiteMapController::class, 'siteMap'])->name('siteMap');
Route::get('/{lang}/sitemap', [SiteMapController::class, 'siteMap'])->name('siteMapByLang');

Route::get('/', [HomeFController::class, 'index'])->name('Home');
// ../home url
Route::get('/home', [HomeFController::class, 'index'])->name('Home');


Route::get('/article/{id}', [ArticleController::class, 'detail'])->name('Article');


Route::get('/{lang?}/home', [HomeController::class, 'HomePageByLang'])->name('HomePageByLang');
// ../subscribe to newsletter submit  (ajax url)
Route::post('/subscribe', [HomeController::class, 'subscribeSubmit'])->name('subscribeSubmit');
// ../Comment submit  (ajax url)
Route::post('/comment', [HomeController::class, 'commentSubmit'])->name('commentSubmit');
// ../Order submit  (ajax url)
Route::post('/order', [HomeController::class, 'orderSubmit'])->name('orderSubmit');
// ..Custom URL for contact us page ( www.site.com/contact )
Route::get('/contact', [HomeController::class, 'ContactPage'])->name('contactPage');
Route::get('/{lang?}/contact', [HomeController::class, 'ContactPageByLang'])->name('contactPageByLang');
// ../contact message submit  (ajax url)
Route::post('/contact/submit', [HomeController::class, 'ContactPageSubmit'])->name('contactPageSubmit');
// ..if page by name ( ex: www.site.com/about )
Route::get('/topic/{id}', [HomeController::class, 'topic'])->name('FrontendPage');
// ..if page by user id ( ex: www.site.com/user )
Route::get('/user/{id}', [HomeController::class, 'userTopics'])->name('FrontendUserTopics');
Route::get('/{lang?}/user/{id}', [HomeController::class, 'userTopicsByLang'])->name('FrontendUserTopicsByLang');
// ../search
Route::post('/search', [HomeController::class, 'searchTopics'])->name('searchTopics');

// ..Topics url  ( ex: www.site.com/news/topic/32 )
Route::get('/{section}/topic/{id}', [HomeController::class, 'topic'])->name('FrontendTopic');
Route::get('/{lang?}/{section}/topic/{id}', [HomeController::class, 'topicByLang'])->name('FrontendTopicByLang');


// ..Sub category url for Section  ( ex: www.site.com/products/2 )
Route::get('/{section}/{cat}', [HomeController::class, 'topics'])->name('FrontendTopicsByCat');
Route::get('/{lang?}/{section}/{cat}', [HomeController::class, 'topicsByLang'])->name('FrontendTopicsByCatWithLang');


// ..Section url by name  ( ex: www.site.com/news )
Route::get('/{section}', [HomeController::class, 'topics'])->name('FrontendTopics');
Route::get('/{lang?}/{section}', [HomeController::class, 'topicsByLang'])->name('FrontendTopicsByLang');


// ..SEO url  ( ex: www.site.com/title-here )
Route::get('/{seo_url_slug}', [HomeController::class, 'SEO'])->name('FrontendSEO');
Route::get('/{lang?}/{seo_url_slug}', [HomeController::class, 'SEOByLang'])->name('FrontendSEOByLang');

// ..if page by name and language( ex: www.site.com/ar/about )
Route::get('/{lang?}/topic/{id}', [HomeController::class, 'topicByLang'])->name('FrontendPageByLang');
// .. End of Frontend Route


// Route::get('/admin', function() {
//     return redirect('https://admin.bioota.com');
// });
Route::get('/login', function() {
    if ($_SERVER['SERVER_NAME'] == '127.0.0.1') {
        return redirect('http://localhost:8001');
    } else {
        return redirect('https://admin.bioota.com');
    }
})->name('login');
Route::get('/admin/login', function() {
    if ($_SERVER['SERVER_NAME'] == '127.0.0.1') {
        return redirect('http://localhost:8001');
    } else {
        return redirect('https://admin.bioota.com');
    }
});