<?php
  if (Request::segment(1) == 'en' || Request::segment(1) == 'id') {
    $lang = Request::segment(1);
  }

  // Dev
  // $base_url = 'http://localhost:8000';
  // $base_url_admin = 'http://localhost:8001';


  // Prod dev.bioota.com
  $base_url = 'https://bioota.com';
  $base_url_admin = 'https://admin.bioota.com';

  // // Prod dev.bioota.com
  // $base_url = 'https://dev.bioota.com';

  $base_url_lang = "https://bioota.com/".@$lang;
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Bioota</title>
    <meta http-equiv="Cache-control" content="public">
    <meta name="description" content="Pengiriman ikan di seluruh dunia." />
    <meta name="keywords" content="key, words, website, web" />
    <meta name="author" content="<?= $base_url ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="<?= $base_url ?>/assets/frontend/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="<?= $base_url ?>/assets/frontend/css/jcarousel.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/flexslider.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/style.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/color.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/colors.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= $base_url ?>/assets/frontend/js/owl-carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= $base_url ?>/assets/frontend/js/owl-carousel/assets/owl.theme.default.min.css">
    <!-- Favicon and Touch Icons -->
    <link href="<?= $base_url ?>/uploads/settings/16448203118360.png" rel="shortcut icon" type="image/png">
    <link href="<?= $base_url ?>/uploads/settings/16448207562325.png" rel="apple-touch-icon">
    <link href="<?= $base_url ?>/uploads/settings/16448207562325.png" rel="apple-touch-icon" sizes="72x72">
    <link href="<?= $base_url ?>/uploads/settings/16448207562325.png" rel="apple-touch-icon" sizes="114x114">
    <link href="<?= $base_url ?>/uploads/settings/16448207562325.png" rel="apple-touch-icon" sizes="144x144">
    <meta property='og:title' content=' ' />
    <meta property='og:image' content='<?= $base_url ?>/uploads/settings/16448207562325.png' />
    <meta property="og:site_name" content="">
    <meta property="og:description" content="Pengiriman ikan di seluruh dunia." />
    <meta property="og:url" content="<?= $base_url ?>" />
    <meta property="og:type" content="website" />
    <style type="text/css">
      a,
      a:hover,
      a:focus,
      a:active,
      footer a.text-link:hover,
      strike,
      .post-meta span a:hover,
      footer a.text-link,
      ul.meta-post li a:hover,
      ul.cat li a:hover,
      ul.recent li h6 a:hover,
      ul.portfolio-categ li.active a,
      ul.portfolio-categ li.active a:hover,
      ul.portfolio-categ li a:hover,
      ul.related-post li h4 a:hover,
      span.highlight,
      article .post-heading h3 a:hover,
      .navbar .nav>.active>a,
      .navbar .nav>.active>a:hover,
      .navbar .nav>li>a:hover,
      .navbar .nav>li>a:focus,
      .navbar .nav>.active>a:focus {
        color: #1ba69f;
      }

      .navbar-brand span {
        color: #1ba69f;
      }

      header .nav li a:hover,
      header .nav li a:focus,
      header .nav li.active a,
      header .nav li.active a:hover,
      header .nav li a.dropdown-toggle:hover,
      header .nav li a.dropdown-toggle:focus,
      header .nav li.active ul.dropdown-menu li a:hover,
      header .nav li.active ul.dropdown-menu li.active a {
        color: #1ba69f;
      }

      .navbar-default .navbar-nav>.active>a,
      .navbar-default .navbar-nav>.active>a:hover,
      .navbar-default .navbar-nav>.active>a:focus {
        color: #1ba69f;
      }

      .navbar-default .navbar-nav>.open>a,
      .navbar-default .navbar-nav>.open>a:hover,
      .navbar-default .navbar-nav>.open>a:focus {
        color: #1ba69f;
      }

      .dropdown-menu>.active>a,
      .dropdown-menu>.active>a:hover,
      .dropdown-menu>.active>a:focus {
        color: #1ba69f;
      }

      .custom-carousel-nav.right:hover,
      .custom-carousel-nav.left:hover,
      .dropdown-menu li:hover,
      .dropdown-menu li a:hover,
      .dropdown-menu li>a:focus,
      .dropdown-submenu:hover>a,
      .dropdown-menu .active>a,
      .dropdown-menu .active>a:hover,
      .pagination ul>.active>a:hover,
      .pagination ul>.active>a,
      .pagination ul>.active>span,
      .flex-control-nav li a:hover,
      .flex-control-nav li a.active {
        background-color: #1ba69f;
      }

      .pagination ul>li.active>a,
      .pagination ul>li.active>span,
      a.thumbnail:hover,
      input[type="text"].search-form:focus {
        border: 1px solid #1ba69f;
      }

      textarea:focus,
      input[type="text"]:focus,
      input[type="password"]:focus,
      input[type="datetime"]:focus,
      input[type="datetime-local"]:focus,
      input[type="date"]:focus,
      input[type="month"]:focus,
      input[type="time"]:focus,
      input[type="week"]:focus,
      input[type="number"]:focus,
      input[type="email"]:focus,
      input[type="url"]:focus,
      input[type="search"]:focus,
      input[type="tel"]:focus,
      input[type="color"]:focus,
      .uneditable-input:focus {
        border-color: #1ba69f;
      }

      input:focus {
        border-color: #1ba69f;
      }

      #sendmessage {
        color: #1ba69f;
      }

      .pullquote-left {
        border-color: #2e3e4e;
      }

      .pullquote-right {
        border-right: 5px solid #1ba69f;
      }

      .cta-text h2 span {
        color: #1ba69f;
      }

      ul.clients li:hover {
        border: 4px solid #1ba69f;
      }

      .box-bottom {
        background: #1ba69f;
      }

      .btn-dark:hover,
      .btn-dark:focus,
      .btn-dark:active {
        background: #1ba69f;
        border: 1px solid #1ba69f;
      }

      .btn-theme {
        border: 1px solid #1ba69f;
        background: #1ba69f;
      }

      .modal.styled .modal-header {
        background-color: #1ba69f;
      }

      .post-meta {
        border-top: 4px solid #1ba69f;
      }

      .post-meta .comments a:hover {
        color: #1ba69f;
      }

      .widget ul.tags li a:hover {
        background: #1ba69f;
      }

      .recent-post .text h5 a:hover {
        color: #1ba69f;
      }

      .pricing-box-alt.special .pricing-heading {
        background: #1ba69f;
      }

      #pagination a:hover {
        background: #1ba69f;
      }

      .pricing-box.special .pricing-offer {
        background: #1ba69f;
      }

      .icon-square:hover,
      .icon-rounded:hover,
      .icon-circled:hover {
        background-color: #1ba69f;
      }

      [class^="icon-"].active,
      [class*=" icon-"].active {
        background-color: #1ba69f;
      }

      .fancybox-close:hover {
        background-color: #1ba69f;
      }

      .fancybox-nav:hover span {
        background-color: #1ba69f;
      }

      .da-slide .da-link:hover {
        background: #1ba69f;
        border: 4px solid #1ba69f;
      }

      .da-dots span {
        background: #1ba69f;
      }

      #featured .flexslider .slide-caption {
        border-left: 5px solid #1ba69f;
      }

      .nivo-directionNav a:hover {
        background-color: #1ba69f;
      }

      .nivo-caption,
      .caption {
        border-bottom: #1ba69f 5px solid;
      }

      footer {
        background: #1ba69f;
      }

      #sub-footer {
        background: #2e3e4e;
      }

      .site-top {
        background: #1ba69f;
      }

      ul.cat li .active {
        color: #1ba69f;
      }

      .box-gray .icon .fa,
      h4,
      .heading {
        color: #1ba69f;
      }

      .flex-caption {
        background-color: #2e3e4e;
      }

      .flex-caption .btn-theme {
        background: #1ba69f;
      }

      .flex-caption .btn-theme:hover {
        background: #fff;
        color: #1ba69f;
      }

      .btn-info {
        background: #1ba69f;
        border-color: #2e3e4e;
      }

      .btn-info:hover {
        background: #2e3e4e;
      }

      .flex-control-paging li a.flex-active {
        background: #1ba69f;
      }

      .flex-control-paging li a {
        background: #2e3e4e;
      }

      .navbar-default .navbar-nav>.active>a,
      .navbar-default .navbar-nav>.active>a:hover,
      .navbar-default .navbar-nav>.active>a:focus {
        background: transparent;
      }

      .navbar-default .navbar-nav>.open>a,
      .navbar-default .navbar-nav>.open>a:hover,
      .navbar-default .navbar-nav>.open>a:focus {
        background: transparent;
      }

      #inner-headline {
        background: #2e3e4e;
      }

      .navbar .nav li .dropdown-menu {
        background: #1ba69f;
      }

      .navbar .nav li .dropdown-menu li:hover {
        background: #2e3e4e;
      }

      @media (max-width: 767px) {
        header .navbar-nav>li {
          background: #1ba69f;
        }

        .dropdown-menu {
          background: #1ba69f;
        }

        .navbar-default .navbar-nav .open .dropdown-menu>li>a {
          color: #fff;
        }

        header .nav li a:hover,
        header .nav li a:focus,
        header .nav li.active a,
        header .nav li.active a:hover,
        header .nav li a.dropdown-toggle:hover,
        header .nav li a.dropdown-toggle:focus,
        header .nav li.active ul.dropdown-menu li a:hover,
        header .nav li.active ul.dropdown-menu li.active a {
          color: #fff;
        }

        .navbar .nav>li>a {
          color: #fff;
        }

        .navbar-default .navbar-nav>.active>a,
        .navbar-default .navbar-nav>.active>a:hover,
        .navbar-default .navbar-nav>.active>a:focus {
          background-color: #1ba69f;
          color: #fff;
        }

        .navbar-default .navbar-nav>.open>a,
        .navbar-default .navbar-nav>.open>a:hover,
        .navbar-default .navbar-nav>.open>a:focus {
          background-color: #1ba69f;
          color: #fff;
        }
      }

      .navbar .nav>li:hover>a,
      .pagination>li>a,
      .pagination>li>a:hover,
      .pagination>li>a:active,
      .pagination>li>a:focus {
        color: #1ba69f;
      }

      .content-row-bg {
        background: #2e3e4e;
      }

      #content .contacts p .fa,
      #content .contacts address .fa {
        background: #1ba69f;
      }

      .pagination>.active>a,
      .pagination>.active>span,
      .pagination>.active>a:hover,
      .pagination>.active>span:hover,
      .pagination>.active>a:focus,
      .pagination>.active>span:focus {
        background-color: #1ba69f;
        border-color: #1ba69f;
      }

      ::-webkit-scrollbar-thumb {
        background: #2e3e4e;
      }

      ::-webkit-scrollbar-thumb:hover {
        background: #1ba69f;
      }
    </style>
  </head>
  <body class="js " style="  ">
    <div id="wrapper">
      <!-- start preloader -->
      <div id="preloader" style="
        height: 100%;
        width: 100%;
    ">
        <div style="
        position: fixed;
        left: 0;
        bottom: 10%;
        z-index: 9999999;
        width: 100%;
        height: 100%;
        overflow: visible;
        background: #fff url('/assets/frontend/img/Preloader.gif') no-repeat center center;
    "></div>
      </div>
      <script>
        function hidePreloader() {
          $('#preloader').fadeOut();
        }
      </script>
      <!-- end preloader -->
      <!-- start header -->
      <header>
        <div class="site-top">
          <div class="container">
            <div>
              <div class="pull-right">
                <!-- <strong><a href="<?= $base_url ?>/admin"><i
                                        class="fa fa-cog"></i> Dashboard
                                </a></strong> -->
                <div class="btn-group header-dropdown">
                  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="<?= $base_url ?>/assets/dashboard/images/flags/us.svg" alt=""> English <i class="fa fa-angle-down"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a href="<?= $base_url ?>/lang/en" class="dropdown-item">
                      <img src="<?= $base_url ?>/assets/dashboard/images/flags/us.svg" alt=""> English </a>
                    <a href="<?= $base_url ?>/lang/id" class="dropdown-item">
                      <img src="<?= $base_url ?>/assets/dashboard/images/flags/id.svg" alt=""> Indonesia </a>
                  </div>
                </div>
              </div>
              <div class="pull-left">
                <!-- <i class="fa fa-phone"></i> &nbsp;<a
                            href="tel:+6231 8474117 - 8474119"><span
                                dir="ltr">+6231 8474117 - 8474119</span></a> -->
                <span class="profile_phone"></span>
                <span class="top-email"> &nbsp; | &nbsp; <i class="fa fa-envelope"></i> &nbsp;
                  <!-- <a
                            href="mailto:marketing@bioota.com">marketing@bioota.com
                        </a> -->
                  <span class="profile_email"></span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="navbar navbar-default">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?= $base_url ?>">
                <img alt="" src="<?= $base_url ?>/uploads/settings/16463594737711.png">
              </a>
            </div>
            <div class="navbar-collapse collapse ">
              <ul class="nav navbar-nav">
                <li>
                  <a href="<?= $base_url ?>/home">Home</a>
                </li>
                <li>
                  <a href="<?= $base_url ?>/products">Products</a>
                </li>
                <li>
                  <a href="<?= $base_url ?>/topic/about">About</a>
                </li>
                <li>
                  <a href="<?= $base_url ?>/contact">Contact</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
      <!-- end header -->
      <!-- Content Section -->
      <div class="contents">
        <!-- start Home Slider -->
        <section id="featured">
          <!-- start slider -->
          <!-- Slider -->
          <div id="main-slider" class="flexslider">
            <ul class="slides">
              <li>
                <img id="banner1" alt="Banner 1" />
                <!-- <div class="flex-caption">
                  <h3>Milkfish is</h3>
                  <p>It is a long established fact that a reader will be distracted by the readable content of a page.</p>
                  <a href="#" class="btn btn-theme">More Details</a>
                </div> -->
              </li>
              <li>
                <img id="banner2" alt="Banner 2" />
                <!-- <div class="flex-caption">
                  <h3>History Milkfish</h3>
                  <p>It is a long established fact that a reader will be distracted by the readable content of a page.</p>
                  <a href="#" class="btn btn-theme">More Details</a>
                </div> -->
              </li>
            </ul>
          </div>
          <!-- end slider -->
        </section>
        <!-- end Home Slider -->
        <section class="content-row-no-bg home-welcome">
          <div class="container">
            <div style='text-align: center'>
              <span>-</span>
              <h2 id="company_title">Welcome to our website</h2>
              <span id="company_content">
              It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.It is a long established fact that a reader will be distracted by the readable content of a page.
              </span>
            </div>
          </div>
        </section>
        <section class="content-row-no-bg">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <div class="home-row-head">
                  <!-- <h2 class="heading">Recent Works</h2><small>Some of our latest works, you can browse more</small> -->
                </div>
                <div class="row">
                  <section id="projects">
                    <ul id="thumbs" class="portfolio"></ul>
                  </section>
                </div>
                <!-- <div class="row"><div class="col-lg-12"><div class="more-btn"><a href="<?= $base_url ?>/photos" class="btn btn-theme"><i
                                            class="fa fa-angle-left"></i>&nbsp; View More
                                        &nbsp;<i
                                            class="fa fa-angle-right"></i></a></div></div></div> -->
              </div>
            </div>
          </div>
        </section>
        
        <section class="content-row-bg">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <div class="home-row-head">
                  <h2 class="heading">Latest Articles</h2>
                  <small>The latest articles from our blog, you can browse more</small>
                </div>
                <div id="owl-slider_" class="owl-carousel owl-theme listing owl-loaded owl-drag">
                  <div class="owl-stage-outer">
                    <div class="owl-stage row" id="div-article">
                    </div>
                  </div>
                  <div class="owl-nav disabled">
                    <div class="owl-prev">prev</div>
                    <div class="owl-next">next</div>
                  </div>
                  <!-- <div class="owl-dots">
                    <div class="owl-dot">
                      <span></span>
                    </div>
                    <div class="owl-dot active">
                      <span></span>
                    </div>
                    <div class="owl-dot">
                      <span></span>
                    </div>
                  </div> -->
                </div>
              </div>
            </div>
            <!-- <div class="row">
              <div class="col-lg-12">
                <div class="more-btn">
                  <a href="https://smartend.app/demo/en/blog" class="btn btn-theme">
                    <i class="fa fa-angle-left"></i>&nbsp; View More &nbsp; <i class="fa fa-angle-right"></i>
                  </a>
                </div>
              </div>
            </div> -->
          </div>
        </section>

      </div>
      <!-- end of Content Section -->
      <!-- start footer -->
      <footer>
        <!-- <div class="container"><div class="row"><div class="col-lg-4"><div class="widget contacts"><h4 class="widgetheading"><i
                                class="fa fa-phone-square"></i>&nbsp; Contact Details</h4><address><strong>Address:</strong><br><i class="fa fa-map-marker"></i>
                                &nbsp;AMG Tower Jl. Dukuh Menanggal No.1 A, Dukuh Menanggal, Kec. Gayungan, Kota SBY, Jawa Timur 60234 <br><br><strong>Factory Address:</strong><br><i class="fa fa-map-marker"></i>
                                &nbsp;Jl. daendels KM 83 NO 89 Ds. Sedayulawas, Kec. Brondong, Kab. Lamongan, Jawa Timur
                            </address><p><strong>Call Us:</strong><br><i class="fa fa-phone"></i> &nbsp;<a
                                    href="tel:+6231 8474117 - 8474119"><span
                                        dir="ltr">+6231 8474117 - 8474119</span></a></p><p><strong>Email:</strong><br><i class="fa fa-envelope"></i> &nbsp;<a
                                    href="mailto:marketing@bioota.com">marketing@bioota.com</a></p></div></div><div class="col-lg-4"><div class="widget"><h4 class="widgetheading"><i
                                    class="fa fa-rss"></i>&nbsp; Latest News
                            </h4><ul class="link-list"><li><a href="<?= $base_url ?>/news/topic/24">Fish harvesting process</a></li></ul></div></div><div class="col-lg-4"><div class="widget"><h4 class="widgetheading"><i
                                        class="fa fa-bookmark"></i>&nbsp; Quick Links</h4><ul class="link-list"><li><a href="<?= $base_url ?>/home">Home</a></li><li><a href="<?= $base_url ?>/products">Products</a></li><li><a href="<?= $base_url ?>/topic/about">About</a></li><li><a href="<?= $base_url ?>/contact">Contact</a></li></ul></div></div></div></div> -->
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div class="widget contacts">
                <h4 class="widgetheading">
                  <i class="fa fa-phone-square"></i>&nbsp; Contact Details
                </h4>
                <address>
                  <strong>Address:</strong>
                  <br>
                  <i class="fa fa-map-marker"></i> &nbsp; <span class="profile_address"></span>
                  <br>
                  <br>
                  <strong>Factory Address:</strong>
                  <br>
                  <i class="fa fa-map-marker"></i> &nbsp; <span class="profile_address2"></span>
                </address>
                <p>
                  <strong>Call Us:</strong>
                  <br>
                  <i class="fa fa-phone"></i> &nbsp; <span class="profile_phone"></span>
                </p>
                <p>
                  <strong>Email:</strong>
                  <br>
                  <i class="fa fa-envelope"></i> &nbsp; <span class="profile_email"></span>
                </p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="widget">
                <h4 class="widgetheading">
                  <i class="fa fa-rss"></i>&nbsp; Latest News
                </h4>
                <ul class="link-list">
                  <li>
                    <a href="<?= $base_url ?>/news/topic/24">Fish harvesting process</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="widget">
                <h4 class="widgetheading">
                  <i class="fa fa-bookmark"></i>&nbsp; Quick Links
                </h4>
                <ul class="link-list">
                  <li>
                    <a href="<?= $base_url ?>/home">Home</a>
                  </li>
                  <li>
                    <a href="<?= $base_url ?>/product">Product</a>
                  </li>
                  <li>
                    <a href="<?= $base_url ?>/about">About</a>
                  </li>
                  <li>
                    <a href="<?= $base_url ?>/contact">Contact</a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- <div class="col-lg-3"><div class="widget"><h4 class="widgetheading"><i class="fa fa-envelope-open"></i>&nbsp; Our Newsletter
                </h4><p>Subscribe To Our Newsletter</p><div id="subscribesendmessage"><i class="fa fa-check-circle"></i> &nbsp;You have Subscribed
                </div><div id="subscribeerrormessage">Error: Please try again</div><form method="POST" action="<?= $base_url ?>/" accept-charset="UTF-8" class="subscribeForm"><input name="_token" type="hidden" value="1aQ4yV2Vf8kQHVqweteMCK81FOV2JhelZmEMHRTo"><div class="form-group"><input placeholder="Your Name" class="form-control" id="subscribe_name" data-msg="Please enter your name" data-rule="minlen:4" name="subscribe_name" type="text" value=""><div class="alert alert-warning validation"></div></div><div class="form-group"><input placeholder="Your Email" class="form-control" id="subscribe_email" data-msg="Please enter your email address" data-rule="email" name="subscribe_email" type="email" value=""><div class="validation"></div></div><button type="submit" class="btn btn-info">Subscribe</button></form></div></div> -->
          </div>
        </div>
        <div id="sub-footer">
          <div class="container">
            <div class="row">
              <div class="col-lg-6">
                <div class="copyright"> &copy; 2023 All Rights Reserved . <a>Bioota</a>
                </div>
              </div>
              <div class="col-lg-6">
                <!-- <ul class="social-network"><li><a href="#" data-placement="top" title="Facebook"
                                   target="_blank"><i
                                        class="fa fa-facebook"></i></a></li><li><a href="#" data-placement="top" title="Twitter"
                                   target="_blank"><i
                                        class="fa fa-twitter"></i></a></li><li><a href="#" data-placement="top" title="Google+"
                                   target="_blank"><i
                                        class="fa fa-google-plus"></i></a></li><li><a href="#" data-placement="top" title="linkedin"
                                   target="_blank"><i
                                        class="fa fa-linkedin"></i></a></li><li><a href="#" data-placement="top" title="Youtube"
                                   target="_blank"><i
                                        class="fa fa-youtube-play"></i></a></li><li><a href="#" data-placement="top" title="Instagram"
                                   target="_blank"><i
                                        class="fa fa-instagram"></i></a></li><li><a href="#" data-placement="top" title="Pinterest"
                                   target="_blank"><i
                                        class="fa fa-pinterest"></i></a></li><li><a href="#" data-placement="top" title="Tumblr"
                                   target="_blank"><i
                                        class="fa fa-tumblr"></i></a></li><li><a href="#" data-placement="top" title="Snapchat"
                                   target="_blank"><i
                                        class="fa fa-snapchat"></i></a></li><li><a href="https://api.whatsapp.com/send?phone=#"
                                   data-placement="top"
                                   title="Whatsapp" target="_blank"><i
                                        class="fa fa-whatsapp"></i></a></li></ul> -->
              </div>
            </div>
          </div>
        </div>
      </footer>
      <script src="<?= $base_url ?>/assets/frontend/js/jquery.js"></script>
      <script>
        $(document).ready(function() {
          getHome()
          getArticles()
          getProfile()
          // getSettingCaret();
        });

        function getHome() {
          return $.ajax({
            // 'url' : `${base_url}/api/profile`,
            'url': `<?= $base_url ?>/api/home`,
            'type': 'GET',
            'success': function(res) {
              if (res.success) {
                home = res.data[0];
                $('#company_title').html(home.company_title)
                $('#company_content').html(home.company_content)
                // alert('<?= $base_url_admin ?>'+'/storage/'+home.banner1)
                $('#banner1').attr('src', '<?= $base_url_admin ?>'+'/storage/'+home.banner1)
                $('#banner2').attr('src', '<?= $base_url_admin ?>'+'/storage/'+home.banner2)
              }
            },
            'error': function(request, error) {
              alert("Request: " + JSON.stringify(request));
            }
          });
        }

        function getArticles() {
          $.ajax({
            // 'url' : `${base_url}/api/profile`,
            'url': `<?= $base_url ?>/api/article`,
            'type': 'GET',
            'success': function(res) {
              if (res.success) {
                articles = res.data;
                var html = '';
                
                $.each(articles, function(k, v) {
                  html += `<div class="owl-item active col-md-4" style="width: 376.667px;">
                    <div class="item">
                      <h4>${v.title}</h4>
                      <img src="<?= $base_url_admin ?>/storage/${v.image}" alt="${v.content}" style="height: 200px; object-fit:cover">
                      <p class="text-justify">
                        ${v.content.substring(0, 100)}
                        <a href="<?= $base_url ?>/article/${v.id}">Read More <i class="fa fa-caret-right"></i>
                        </a>
                      </p>
                    </div>
                  </div>`;
                })
                // alert(html)
                $('#div-article').html(html)
              }
            },
            'error': function(request, error) {
              alert("Request: " + JSON.stringify(request));
            }
          });
        }

        function getProfile() {
          return $.ajax({
            // 'url' : `${base_url}/api/profile`,
            'url': `<?= $base_url ?>/api/profile`,
            'type': 'GET',
            'success': function(res) {
              if (res.success) {
                profile = res.data[0];
                $('.profile_address').html(profile.address)
                $('.profile_address2').html(profile.address2)
                $('.profile_phone').html(profile.phone)
                $('.profile_phone_href').html(`
																								<a href="tel:${profile.phone}">
																									<span dir="ltr">${profile.phone}</span>
																								</a>`);
                $('.profile_email').html(profile.email)
                $('.profile_email_href').html(`
																								<a href="mailto:${profile.email}">${profile.email}</a>`);
                $('.profile_fax').html(profile.fax)
                $('.profile_working_times').html(profile.working_times)
              }
            },
            'error': function(request, error) {
              alert("Request: " + JSON.stringify(request));
            }
          });
        }
      </script>
      <!-- end footer -->
    </div>
    <a href="#" title="to Top" class="scrollup">
      <i class="fa fa-angle-up active"></i>
    </a>
    <script type="text/javascript">
      var page_dir = "ltr";
    </script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.easing.1.3.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/bootstrap.min.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.fancybox.pack.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.fancybox-media.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/google-code-prettify/prettify.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/portfolio/jquery.quicksand.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/portfolio/setting.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.flexslider.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/animate.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/custom.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/owl-carousel/owl.carousel.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/Chart.min.js"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
            "use strict";
            setTimeout(hidePreloader, 2000);
            //Subscribe
            $('form.subscribeForm').submit(function() {
                var f = $(this).find('.form-group'),
                  ferror = false,
                  emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;
                f.children('input').each(function() { // run all inputs
                    var i = $(this); // current input
                    var rule = i.attr('data-rule');
                    if (rule !== undefined) {
                      var ierror = false; // error flag for current input
                      var pos = rule.indexOf(':', 0);
                      if (pos >= 0) {
                        var exp = rule.substr(pos + 1, rule.length);
                        rule = rule.substr(0, pos);
                      } else {
                        rule = rule.substr(pos + 1, rule.length);
                      }
                      switch (rule) {
                        case 'required':
                          if (i.val() === '') {
                            ferror = ierror = true;
                          }
                          break;
                        case 'minlen':
                          if (i.val().length < parseInt(exp)) {
                            ferror = ierror = true;
                          }
                          break;
                        case 'email':
                          if (!emailExp.test(i.val())) {
                            ferror = ierror = true;
                          }
                          break;
                        case 'checked':
                          if (!i.attr('checked')) {
                            ferror = ierror = true;
                          }
                          break;
                        case 'regexp':
                          exp = new RegExp(exp);
                          if (!exp.test(i.val())) {
                            ferror = ierror = true;
                          }
                          break;
                      }
                      i.next('.validation').html(' < i class = \"fa fa-info\"></i> &nbsp;' + ( ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '' )).show();!ierror ? i.next('.validation').hide() : i.next('.validation').show();
                      }
                    });
                  if (ferror) return false;
                  else var str = $(this).serialize(); $.ajax({
                    type: "POST",
                    url: "<?= $base_url ?>/subscribe",
                    data: str,
                    success: function(msg) {
                      if (msg == 'OK') {
                        $("#subscribesendmessage").addClass("show");
                        $("#subscribeerrormessage").removeClass("show");
                        $("#subscribe_name").val('');
                        $("#subscribe_email").val('');
                      } else {
                        $("#subscribesendmessage").removeClass("show");
                        $("#subscribeerrormessage").addClass("show");
                        $('#subscribeerrormessage').html(msg);
                      }
                    }
                  });
                  return false;
                });
            });
    </script>
  </body>
</html>