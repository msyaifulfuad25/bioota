<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
      /* Custom */

      /* Remove default bullets */
      ul, #myUL {
        list-style-type: none;
      }

      /* Remove margins and padding from the parent ul */
      #myUL {
        margin: 0;
        padding: 0;
      }

      /* Style the caret/arrow */
      .caret {
        cursor: pointer;
        user-select: none; /* Prevent text selection */
      }

      /* Create the caret/arrow with a unicode, and style it */
      .caret::before {
        content: "\25B6";
        color: black;
        display: inline-block;
        margin-right: 6px;
      }

      /* Rotate the caret/arrow icon when clicked on (using JavaScript) */
      .caret-down::before {
        transform: rotate(90deg);
      }

      /* Hide the nested list */
      .nested {
        display: none;
      }

      /* Show the nested list when the user clicks on the caret/arrow (with JavaScript) */
      .active {
        display: block;
      }
    </style>
</head>
<body>
    <ul id="myUL">
        <li>
            <span class="caret">Demersal</span>
            <ul class="nested">
                <li>Frozen Threadfin Bream (Nemipterus virgatus)</li>
                <li>Frozen Big Eye Fish (Priacanthus tayenus)</li>
                <li>Frozen Golden Snapper/Johns Snapper (Lutjanus johnii)</li>
                <li>Frozen Conger Pike/Frozen Conger Eel (Muraenesox cinereus)</li>
                <li>Frozen Red Snapper (Lutjanus sanguineus)</li>
                <li>Frozen Grouper (Epinephelus sp.)</li>
                <li>Frozen Ribbon Fish/Frozen Wild Hairtail (Trichiurus lepturus)</li>
                <li>Frozen Leather Jacket Fish (Thamnaconus modestus)</li>
                <li>Frozen Black Pomfet (Parastromateus niger)</li>
                <li>Frozen Flat Fish (Paralichthys olivaceus)</li>
                <li>Frozen Threadfin Fish (Eleutheronema rhadinum)</li>
                <li>Frozen Lizard Fish (Saurida tumbil)</li>
                <li>Frozen Giant Trevally (Caranx Ignobilis)</li>
                <li>Frozen Sting Rays (Dasyatis sp.)</li>
                <li>Frozen Longfin Emperor (Lethrinus lentjam)</li>
                <li>Frozen Croaker Fish (Mycthis Miiuy)</li>
                <li>Frozen Seabass (Lates calcarifer)</li>
                <li>Frozen Long Jawed Mackerel (Rastrelliger sp)</li>
                <li>Frozen Parrot Fish (Scarus sp)</li>
                <li>Frozen Common Silver Biddy (Gerres oyena)</li>
                <li>Frozen Ponyfish (Leiognathus equulus)</li>
            </ul>
        </li>
        <li>
            <span class="caret">Pelagic</span>
            <ul class="nested">
                <li>Frozen Long Jawed Mackerel (Rastrelliger sp)</li>
                <li>Frozen Spanish Mackerel (Scomberomerus commerson)</li>
                <li>Frozen Spanish Mackerel Steak (Scomberomerus commerson)</li>
                <li>Frozen Silver Sillago (Sillago Sihama)</li>
                <li>Frozen Frigate Mackerel (Auxis thazard)</li>
                <li>Frozen Queenfish (Scomberoides tala)</li>
            </ul>
        </li>
        <li>
            <span class="caret">Chepalopoda</span>
            <ul class="nested">
                <li>Frozen Octopus (Octopus membranaceus)</li>
                <li>Frozen Cuttlefish (Sepia officinalis)</li>
                <li>Frozen Soft Bone Cuttlefish (Sepia officinalis)</li>
                <li>Frozen Squid (Loligo Sp.)</li>
            </ul>
        </li>
        <li>
            <span class="caret">Shellfish</span>
            <ul class="nested">
                <li>Frozen Comb Pen Shell (Atrina pectinata)</li>
                <li>Frozen Clam (Paphia undulata)</li>
                <li>Frozen Ark Shell (Anadara antiquata)</li>
                <li>Frozen Blood Shell (Anadara Granosa)</li>
                <li>Frozen Scallop (Amusium pleuronectes)</li>
                <li>Frozen Green Mussels (Perna viridis)</li>
            </ul>
        </li>
        <li>
            <span class="caret">Shrimp</span>
            <ul class="nested">
                <li>Vaname Shrimp (Litopenaeus vannamei)</li>
                <li>Tiger Shrimp (Penaeus monodon)</li>
                <li>Slipper Lobster (Panulirus sp)</li>
            </ul>
        </li>
    </ul>
    <script>
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
        }
    </script>
</body>
</html>