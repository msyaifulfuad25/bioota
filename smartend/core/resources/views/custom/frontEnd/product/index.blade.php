<?php
  if (Request::segment(1) == 'en' || Request::segment(1) == 'id') {
    $lang = Request::segment(1);
  }

  // Dev
  // $base_url = 'http://localhost:8000';
  // $base_url_admin = 'http://localhost:8001';
  
  
  // Prod dev.bioota.com
  $base_url = 'https://bioota.com';
  $base_url_admin = 'http://admin.bioota.com';

  // // Prod dev.bioota.com
  // $base_url = 'https://dev.bioota.com';

  $base_url_lang = "https://bioota.com/".@$lang;
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Products | Bioota</title>
    <meta name="description" content="Pengiriman ikan di seluruh dunia." />
    <meta name="keywords" content="key, words, website, web" />
    <meta name="author" content="<?= $base_url ?>/" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="<?= $base_url ?>/assets/frontend/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="<?= $base_url ?>/assets/frontend/css/jcarousel.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/flexslider.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/style.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/color.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/colors.css" rel="stylesheet" />
    <link href="<?= $base_url ?>/assets/frontend/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= $base_url ?>/assets/frontend/js/owl-carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= $base_url ?>/assets/frontend/js/owl-carousel/assets/owl.theme.default.min.css">
    <!-- Favicon and Touch Icons -->
    <link href="<?= $base_url ?>/uploads/settings/16448203118360.png" rel="shortcut icon" type="image/png">
    <link href="<?= $base_url ?>/uploads/settings/16448207562325.png" rel="apple-touch-icon">
    <link href="<?= $base_url ?>/uploads/settings/16448207562325.png" rel="apple-touch-icon" sizes="72x72">
    <link href="<?= $base_url ?>/uploads/settings/16448207562325.png" rel="apple-touch-icon" sizes="114x114">
    <link href="<?= $base_url ?>/uploads/settings/16448207562325.png" rel="apple-touch-icon" sizes="144x144">
    <meta property='og:title' content='Products ' />
    <meta property='og:image' content='<?= $base_url ?>/uploads/topics/16635607192098.png' />
    <meta property="og:site_name" content="">
    <meta property="og:description" content="Pengiriman ikan di seluruh dunia." />
    <meta property="og:url" content="<?= $base_url ?>/products" />
    <meta property="og:type" content="website" />
    <style type="text/css">
      a,
      a:hover,
      a:focus,
      a:active,
      footer a.text-link:hover,
      strike,
      .post-meta span a:hover,
      footer a.text-link,
      ul.meta-post li a:hover,
      ul.cat li a:hover,
      ul.recent li h6 a:hover,
      ul.portfolio-categ li.active a,
      ul.portfolio-categ li.active a:hover,
      ul.portfolio-categ li a:hover,
      ul.related-post li h4 a:hover,
      span.highlight,
      article .post-heading h3 a:hover,
      .navbar .nav>.active>a,
      .navbar .nav>.active>a:hover,
      .navbar .nav>li>a:hover,
      .navbar .nav>li>a:focus,
      .navbar .nav>.active>a:focus {
        color: #1ba69f;
      }

      .navbar-brand span {
        color: #1ba69f;
      }

      header .nav li a:hover,
      header .nav li a:focus,
      header .nav li.active a,
      header .nav li.active a:hover,
      header .nav li a.dropdown-toggle:hover,
      header .nav li a.dropdown-toggle:focus,
      header .nav li.active ul.dropdown-menu li a:hover,
      header .nav li.active ul.dropdown-menu li.active a {
        color: #1ba69f;
      }

      .navbar-default .navbar-nav>.active>a,
      .navbar-default .navbar-nav>.active>a:hover,
      .navbar-default .navbar-nav>.active>a:focus {
        color: #1ba69f;
      }

      .navbar-default .navbar-nav>.open>a,
      .navbar-default .navbar-nav>.open>a:hover,
      .navbar-default .navbar-nav>.open>a:focus {
        color: #1ba69f;
      }

      .dropdown-menu>.active>a,
      .dropdown-menu>.active>a:hover,
      .dropdown-menu>.active>a:focus {
        color: #1ba69f;
      }

      .custom-carousel-nav.right:hover,
      .custom-carousel-nav.left:hover,
      .dropdown-menu li:hover,
      .dropdown-menu li a:hover,
      .dropdown-menu li>a:focus,
      .dropdown-submenu:hover>a,
      .dropdown-menu .active>a,
      .dropdown-menu .active>a:hover,
      .pagination ul>.active>a:hover,
      .pagination ul>.active>a,
      .pagination ul>.active>span,
      .flex-control-nav li a:hover,
      .flex-control-nav li a.active {
        background-color: #1ba69f;
      }

      .pagination ul>li.active>a,
      .pagination ul>li.active>span,
      a.thumbnail:hover,
      input[type="text"].search-form:focus {
        border: 1px solid #1ba69f;
      }

      textarea:focus,
      input[type="text"]:focus,
      input[type="password"]:focus,
      input[type="datetime"]:focus,
      input[type="datetime-local"]:focus,
      input[type="date"]:focus,
      input[type="month"]:focus,
      input[type="time"]:focus,
      input[type="week"]:focus,
      input[type="number"]:focus,
      input[type="email"]:focus,
      input[type="url"]:focus,
      input[type="search"]:focus,
      input[type="tel"]:focus,
      input[type="color"]:focus,
      .uneditable-input:focus {
        border-color: #1ba69f;
      }

      input:focus {
        border-color: #1ba69f;
      }

      #sendmessage {
        color: #1ba69f;
      }

      .pullquote-left {
        border-color: #2e3e4e;
      }

      .pullquote-right {
        border-right: 5px solid #1ba69f;
      }

      .cta-text h2 span {
        color: #1ba69f;
      }

      ul.clients li:hover {
        border: 4px solid #1ba69f;
      }

      .box-bottom {
        background: #1ba69f;
      }

      .btn-dark:hover,
      .btn-dark:focus,
      .btn-dark:active {
        background: #1ba69f;
        border: 1px solid #1ba69f;
      }

      .btn-theme {
        border: 1px solid #1ba69f;
        background: #1ba69f;
      }

      .modal.styled .modal-header {
        background-color: #1ba69f;
      }

      .post-meta {
        border-top: 4px solid #1ba69f;
      }

      .post-meta .comments a:hover {
        color: #1ba69f;
      }

      .widget ul.tags li a:hover {
        background: #1ba69f;
      }

      .recent-post .text h5 a:hover {
        color: #1ba69f;
      }

      .pricing-box-alt.special .pricing-heading {
        background: #1ba69f;
      }

      #pagination a:hover {
        background: #1ba69f;
      }

      .pricing-box.special .pricing-offer {
        background: #1ba69f;
      }

      .icon-square:hover,
      .icon-rounded:hover,
      .icon-circled:hover {
        background-color: #1ba69f;
      }

      [class^="icon-"].active,
      [class*=" icon-"].active {
        background-color: #1ba69f;
      }

      .fancybox-close:hover {
        background-color: #1ba69f;
      }

      .fancybox-nav:hover span {
        background-color: #1ba69f;
      }

      .da-slide .da-link:hover {
        background: #1ba69f;
        border: 4px solid #1ba69f;
      }

      .da-dots span {
        background: #1ba69f;
      }

      #featured .flexslider .slide-caption {
        border-left: 5px solid #1ba69f;
      }

      .nivo-directionNav a:hover {
        background-color: #1ba69f;
      }

      .nivo-caption,
      .caption {
        border-bottom: #1ba69f 5px solid;
      }

      footer {
        background: #1ba69f;
      }

      #sub-footer {
        background: #2e3e4e;
      }

      .site-top {
        background: #1ba69f;
      }

      ul.cat li .active {
        color: #1ba69f;
      }

      .box-gray .icon .fa,
      h4,
      .heading {
        color: #1ba69f;
      }

      .flex-caption {
        background-color: #2e3e4e;
      }

      .flex-caption .btn-theme {
        background: #1ba69f;
      }

      .flex-caption .btn-theme:hover {
        background: #fff;
        color: #1ba69f;
      }

      .btn-info {
        background: #1ba69f;
        border-color: #2e3e4e;
      }

      .btn-info:hover {
        background: #2e3e4e;
      }

      .flex-control-paging li a.flex-active {
        background: #1ba69f;
      }

      .flex-control-paging li a {
        background: #2e3e4e;
      }

      .navbar-default .navbar-nav>.active>a,
      .navbar-default .navbar-nav>.active>a:hover,
      .navbar-default .navbar-nav>.active>a:focus {
        background: transparent;
      }

      .navbar-default .navbar-nav>.open>a,
      .navbar-default .navbar-nav>.open>a:hover,
      .navbar-default .navbar-nav>.open>a:focus {
        background: transparent;
      }

      #inner-headline {
        background: #2e3e4e;
      }

      .navbar .nav li .dropdown-menu {
        background: #1ba69f;
      }

      .navbar .nav li .dropdown-menu li:hover {
        background: #2e3e4e;
      }

      @media (max-width: 767px) {
        header .navbar-nav>li {
          background: #1ba69f;
        }

        .dropdown-menu {
          background: #1ba69f;
        }

        .navbar-default .navbar-nav .open .dropdown-menu>li>a {
          color: #fff;
        }

        header .nav li a:hover,
        header .nav li a:focus,
        header .nav li.active a,
        header .nav li.active a:hover,
        header .nav li a.dropdown-toggle:hover,
        header .nav li a.dropdown-toggle:focus,
        header .nav li.active ul.dropdown-menu li a:hover,
        header .nav li.active ul.dropdown-menu li.active a {
          color: #fff;
        }

        .navbar .nav>li>a {
          color: #fff;
        }

        .navbar-default .navbar-nav>.active>a,
        .navbar-default .navbar-nav>.active>a:hover,
        .navbar-default .navbar-nav>.active>a:focus {
          background-color: #1ba69f;
          color: #fff;
        }

        .navbar-default .navbar-nav>.open>a,
        .navbar-default .navbar-nav>.open>a:hover,
        .navbar-default .navbar-nav>.open>a:focus {
          background-color: #1ba69f;
          color: #fff;
        }
      }

      .navbar .nav>li:hover>a,
      .pagination>li>a,
      .pagination>li>a:hover,
      .pagination>li>a:active,
      .pagination>li>a:focus {
        color: #1ba69f;
      }

      .content-row-bg {
        background: #2e3e4e;
      }

      #content .contacts p .fa,
      #content .contacts address .fa {
        background: #1ba69f;
      }

      .pagination>.active>a,
      .pagination>.active>span,
      .pagination>.active>a:hover,
      .pagination>.active>span:hover,
      .pagination>.active>a:focus,
      .pagination>.active>span:focus {
        background-color: #1ba69f;
        border-color: #1ba69f;
      }

      ::-webkit-scrollbar-thumb {
        background: #2e3e4e;
      }

      ::-webkit-scrollbar-thumb:hover {
        background: #1ba69f;
      }
    </style>

    <style>
      /* Custom */

      /* Remove default bullets */
      ul, #myUL {
        list-style-type: none;
      }

      /* Remove margins and padding from the parent ul */
      #myUL {
        margin: 0;
        padding: 0;
      }

      /* Style the caret/arrow */
      .myCaret {
        cursor: pointer;
        user-select: none; /* Prevent text selection */
      }

      /* Create the caret/arrow with a unicode, and style it */
      .myCaret::before {
        content: "\25B6";
        color: black;
        /* display: inline-block; */
        margin-right: 6px;
      }

      /* Rotate the caret/arrow icon when clicked on (using JavaScript) */
      .myCaret-down::before {
        transform: rotate(90deg);
      }

      /* Hide the nested list */
      .nested {
        display: none;
      }

      /* Show the nested list when the user clicks on the caret/arrow (with JavaScript) */
      .active {
        display: block;
      }

      /* Custom Slider */
      * {
        box-sizing: border-box;
      }

      /* Position the image container (needed to position the left and right arrows) */
      .container {
        position: relative;
      }

      /* Hide the images by default */
      .mySlides {
        display: none;
      }

      /* Add a pointer when hovering over the thumbnail images */
      .cursor {
        cursor: pointer;
      }

      /* Next & previous buttons */
      .prev,
      .next {
        cursor: pointer;
        position: absolute;
        top: 40%;
        width: auto;
        padding: 16px;
        margin-top: -50px;
        color: white;
        font-weight: bold;
        font-size: 20px;
        border-radius: 0 3px 3px 0;
        user-select: none;
        -webkit-user-select: none;
      }

      /* Position the "next button" to the right */
      .next {
        right: 0;
        border-radius: 3px 0 0 3px;
        margin-right: 15px;
      }

      /* On hover, add a black background color with a little bit see-through */
      .prev:hover,
      .next:hover {
        background-color: rgba(0, 0, 0, 0.8);
      }

      /* Number text (1/3 etc) */
      .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
      }

      /* Container for image text */
      .caption-container {
        text-align: center;
        background-color: #222;
        padding: 2px 16px;
        color: white;
      }

      .row:after {
        content: "";
        display: table;
        clear: both;
      }

      /* Six columns side by side */
      .column {
        float: left;
        width: 16.66%;
      }

      /* Add a transparency effect for thumnbail images */
      .demo {
        opacity: 0.6;
      }

      .active,
      .demo:hover {
        opacity: 1;
      }



      .product {
        list-style-type: circle;
        color: rgb(101, 101, 101);
      }

      .product_active {
        color: orange;
      }
    </style>
  </head>
  <body class="js " style="  ">
    <div id="wrapper">
      <!-- start preloader -->
      <div id="preloader" style="
        height: 100%;
        width: 100%;
    ">
        <div style="
        position: fixed;
        left: 0;
        bottom: 10%;
        z-index: 9999999;
        width: 100%;
        height: 100%;
        overflow: visible;
        background: #fff url('/assets/frontend/img/Preloader.gif') no-repeat center center;
    "></div>
      </div>
      <script>
        function hidePreloader() {
          $('#preloader').fadeOut();
        }
      </script>
      <!-- end preloader -->
      <!-- start header -->
      <header>
        <div class="site-top">
          <div class="container">
            <div>
              <div class="pull-right">
                <div class="btn-group header-dropdown">
                  <!-- <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user"></i> admin <i class="fa fa-angle-down"></i>
                  </button> -->
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?= $base_url ?>/admin">
                      <i class="fa fa-cog"></i> Dashboard </a>
                    <a class="dropdown-item" href="<?= $base_url ?>/admin/users/1/edit">
                      <i class="fa fa-user"></i> Profile </a>
                    <a href="<?= $base_url ?>/admin/webmails" class="dropdown-item">
                      <i class="fa fa-envelope"></i> Inbox </a>
                    <a onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item" href="<?= $base_url ?>/logout">
                      <i class="fa fa-sign-out"></i> Logout </a>
                    <form id="logout-form" action="<?= $base_url ?>/logout" method="POST" style="display: none;">
                      <input type="hidden" name="_token" value="1aQ4yV2Vf8kQHVqweteMCK81FOV2JhelZmEMHRTo">
                    </form>
                  </div>
                </div>
                <div class="btn-group header-dropdown">
                  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php if (@$lang == 'id') { ?>
                      <img src="<?= $base_url ?>/assets/dashboard/images/flags/id.svg" alt=""> Indonesia <i class="fa fa-angle-down"></i>
                      <?php } else { ?>
                        <img src="<?= $base_url ?>/assets/dashboard/images/flags/us.svg" alt=""> English <i class="fa fa-angle-down"></i>
                    <?php } ?>
                  </button>
                  <div class="dropdown-menu">
                    <a href="<?= $base_url ?>/lang/en" class="dropdown-item">
                      <img src="<?= $base_url ?>/assets/dashboard/images/flags/us.svg" alt=""> English </a>
                    <a href="<?= $base_url ?>/lang/id" class="dropdown-item">
                      <img src="<?= $base_url ?>/assets/dashboard/images/flags/id.svg" alt=""> Indonesia </a>
                  </div>
                </div>
              </div>
              <div class="pull-left">
                <span class="profile_phone">

                </span>
                <span class="top-email"> &nbsp; | &nbsp; <i class="fa fa-envelope"></i> &nbsp; 
                  <span class="profile_email"></span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="navbar navbar-default">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?= $base_url ?>/">
                <img alt="" src="<?= $base_url ?>/uploads/settings/16463594737711.png">
              </a>
            </div>
            <div class="navbar-collapse collapse ">
              <ul class="nav navbar-nav">
                <li>
                  <a href="<?= $base_url_lang ?>home">Home</a>
                </li>
                <li>
                  <a href="<?= $base_url_lang ?>products">Products</a>
                </li>
                <li>
                  <a href="<?= $base_url_lang ?>topic/about">About</a>
                </li>
                <li>
                  <a href="<?= $base_url_lang ?>contact">Contact</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
      <!-- end header -->
      <!-- Content Section -->
      <div class="contents">
        <section id="inner-headline">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <ul class="breadcrumb">
                  <li>
                    <a href="<?= $base_url ?>/">
                      <i class="fa fa-home"></i>
                    </a>
                    <i class="icon-angle-right"></i>
                  </li>
                  <li class="active">Products</li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <section id="content">
          <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                  <ul id="myUL">
                    
                      <!-- <li>
                          <span class="myCaret"><b>Demersal</b></span>
                          <ul class="nested">
                              <li onclick="viewProduct({
                                id: 0,
                                name: 'Frozen Threadfin Bream (Nemipterus virgatus)',
                                detail: {
                                  processing_type: 'Frozen Whole Round, Frozen Head Cutted',
                                  source: 'Wild Caught',
                                  catch_area: 'FAO 71/WRP RI-712',
                                  packing: 'Primary (Plastics), Secondary (Carton)',
                                  production_mode: 'ABF and CPF',
                                  size: {
                                    0: 'HEAD CUTTED 30-50, 50-100, 100-150, (GR/PCS)',
                                    1: 'WR 80-100, 100-150, 150-200, 200-UP (GR/PCS)'
                                  }
                                }
                              })">Frozen Threadfin Bream (Nemipterus virgatus)</li>
                              <li>Frozen Big Eye Fish (Priacanthus tayenus)</li>
                              <li>Frozen Golden Snapper/Johns Snapper (Lutjanus johnii)</li>
                              <li>Frozen Conger Pike/Frozen Conger Eel (Muraenesox cinereus)</li>
                              <li>Frozen Red Snapper (Lutjanus sanguineus)</li>
                              <li>Frozen Grouper (Epinephelus sp.)</li>
                              <li>Frozen Ribbon Fish/Frozen Wild Hairtail (Trichiurus lepturus)</li>
                              <li>Frozen Leather Jacket Fish (Thamnaconus modestus)</li>
                              <li>Frozen Black Pomfet (Parastromateus niger)</li>
                              <li>Frozen Flat Fish (Paralichthys olivaceus)</li>
                              <li>Frozen Threadfin Fish (Eleutheronema rhadinum)</li>
                              <li>Frozen Lizard Fish (Saurida tumbil)</li>
                              <li>Frozen Giant Trevally (Caranx Ignobilis)</li>
                              <li>Frozen Sting Rays (Dasyatis sp.)</li>
                              <li>Frozen Longfin Emperor (Lethrinus lentjam)</li>
                              <li>Frozen Croaker Fish (Mycthis Miiuy)</li>
                              <li>Frozen Seabass (Lates calcarifer)</li>
                              <li>Frozen Long Jawed Mackerel (Rastrelliger sp)</li>
                              <li>Frozen Parrot Fish (Scarus sp)</li>
                              <li>Frozen Common Silver Biddy (Gerres oyena)</li>
                              <li>Frozen Ponyfish (Leiognathus equulus)</li>
                          </ul>
                      </li>
                      <li>
                          <span class="myCaret"><b>Pelagic</b></span>
                          <ul class="nested">
                              <li>Frozen Long Jawed Mackerel (Rastrelliger sp)</li>
                              <li>Frozen Spanish Mackerel (Scomberomerus commerson)</li>
                              <li>Frozen Spanish Mackerel Steak (Scomberomerus commerson)</li>
                              <li>Frozen Silver Sillago (Sillago Sihama)</li>
                              <li>Frozen Frigate Mackerel (Auxis thazard)</li>
                              <li>Frozen Queenfish (Scomberoides tala)</li>
                          </ul>
                      </li>
                      <li>
                          <span class="myCaret"><b>Chepalopoda</b></span>
                          <ul class="nested">
                              <li>Frozen Octopus (Octopus membranaceus)</li>
                              <li>Frozen Cuttlefish (Sepia officinalis)</li>
                              <li>Frozen Soft Bone Cuttlefish (Sepia officinalis)</li>
                              <li>Frozen Squid (Loligo Sp.)</li>
                          </ul>
                      </li>
                      <li>
                          <span class="myCaret"><b>Shellfish</b></span>
                          <ul class="nested">
                              <li>Frozen Comb Pen Shell (Atrina pectinata)</li>
                              <li>Frozen Clam (Paphia undulata)</li>
                              <li>Frozen Ark Shell (Anadara antiquata)</li>
                              <li>Frozen Blood Shell (Anadara Granosa)</li>
                              <li>Frozen Scallop (Amusium pleuronectes)</li>
                              <li>Frozen Green Mussels (Perna viridis)</li>
                          </ul>
                      </li>
                      <li>
                          <span class="myCaret"><b>Shrimp</b></span>
                          <ul class="nested">
                              <li>Vaname Shrimp (Litopenaeus vannamei)</li>
                              <li>Tiger Shrimp (Penaeus monodon)</li>
                              <li>Slipper Lobster (Panulirus sp)</li>
                          </ul>
                      </li> -->
                  </ul>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9" style="border-left: black solid 1px">
                  <div id="product_view">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
      <!-- end of Content Section -->
      <!-- start footer -->
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div class="widget contacts">
                <h4 class="widgetheading">
                  <i class="fa fa-phone-square"></i>&nbsp; Contact Details
                </h4>
                <address>
                  <strong>Address:</strong>
                  <br>
                  <i class="fa fa-map-marker"></i>
                  &nbsp;<span class="profile_address"></span><br><br>
                  
                  <strong>Factory Address:</strong>
                  <br>
                  <i class="fa fa-map-marker"></i>
                  &nbsp;<span class="profile_address2"></span>
                </address>
                <p>
                  <strong>Call Us:</strong>
                  <br>
                  <i class="fa fa-phone"></i> &nbsp; 
                    <span class="profile_phone">
                    </span>
                </p>
                <p>
                  <strong>Email:</strong>
                  <br>
                  <i class="fa fa-envelope"></i> &nbsp; 
                  <span class="profile_email"></span>
                </p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="widget">
                <h4 class="widgetheading">
                  <i class="fa fa-rss"></i>&nbsp; Latest News
                </h4>
                <ul class="link-list">
                  <li>
                    <a href="<?= $base_url ?>/news/topic/24">Fish harvesting process</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="widget">
                <h4 class="widgetheading">
                  <i class="fa fa-bookmark"></i>&nbsp; Quick Links
                </h4>
                <ul class="link-list">
                  <li>
                    <a href="<?= $base_url ?>/home">Home</a>
                  </li>
                  <li>
                    <a href="<?= $base_url ?>/product">Product</a>
                  </li>
                  <li>
                    <a href="<?= $base_url ?>/about">About</a>
                  </li>
                  <li>
                    <a href="<?= $base_url ?>/contact">Contact</a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- <div class="col-lg-3">
              <div class="widget">
                <h4 class="widgetheading">
                  <i class="fa fa-envelope-open"></i>&nbsp; Our Newsletter
                </h4>
                <p>Subscribe To Our Newsletter</p>
                <div id="subscribesendmessage">
                  <i class="fa fa-check-circle"></i> &nbsp;You have Subscribed
                </div>
                <div id="subscribeerrormessage">Error: Please try again</div>
                <form method="POST" action="<?= $base_url ?>/" accept-charset="UTF-8" class="subscribeForm">
                  <input name="_token" type="hidden" value="1aQ4yV2Vf8kQHVqweteMCK81FOV2JhelZmEMHRTo">
                  <div class="form-group">
                    <input placeholder="Your Name" class="form-control" id="subscribe_name" data-msg="Please enter your name" data-rule="minlen:4" name="subscribe_name" type="text" value="">
                    <div class="alert alert-warning validation"></div>
                  </div>
                  <div class="form-group">
                    <input placeholder="Your Email" class="form-control" id="subscribe_email" data-msg="Please enter your email address" data-rule="email" name="subscribe_email" type="email" value="">
                    <div class="validation"></div>
                  </div>
                  <button type="submit" class="btn btn-info">Subscribe</button>
                </form>
              </div>
            </div> -->
          </div>
        </div>
        <div id="sub-footer">
          <div class="container">
            <div class="row">
              <div class="col-lg-6">
                <div class="copyright"> &copy; 2022 All Rights Reserved . <a>Bioota</a>
                </div>
              </div>
              <div class="col-lg-6">
                <!-- <ul class="social-network">
                  <li>
                    <a href="#" data-placement="top" title="Facebook" target="_blank">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-placement="top" title="Twitter" target="_blank">
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-placement="top" title="Google+" target="_blank">
                      <i class="fa fa-google-plus"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-placement="top" title="linkedin" target="_blank">
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-placement="top" title="Youtube" target="_blank">
                      <i class="fa fa-youtube-play"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-placement="top" title="Instagram" target="_blank">
                      <i class="fa fa-instagram"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-placement="top" title="Pinterest" target="_blank">
                      <i class="fa fa-pinterest"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-placement="top" title="Tumblr" target="_blank">
                      <i class="fa fa-tumblr"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-placement="top" title="Snapchat" target="_blank">
                      <i class="fa fa-snapchat"></i>
                    </a>
                  </li>
                  <li>
                    <a href="https://api.whatsapp.com/send?phone=#" data-placement="top" title="Whatsapp" target="_blank">
                      <i class="fa fa-whatsapp"></i>
                    </a>
                  </li>
                </ul> -->
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- end footer -->
    </div>
    <a href="#" title="to Top" class="scrollup">
      <i class="fa fa-angle-up active"></i>
    </a>
    <script type="text/javascript">
      var page_dir = "ltr";
    </script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.easing.1.3.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/bootstrap.min.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.fancybox.pack.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.fancybox-media.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/google-code-prettify/prettify.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/portfolio/jquery.quicksand.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/portfolio/setting.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/jquery.flexslider.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/animate.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/custom.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/owl-carousel/owl.carousel.js"></script>
    <script src="<?= $base_url ?>/assets/frontend/js/Chart.min.js"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
            "use strict";
            setTimeout(hidePreloader, 2000);
            //Subscribe
            $('form.subscribeForm').submit(function() {
                var f = $(this).find('.form-group'),
                  ferror = false,
                  emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;
                f.children('input').each(function() { // run all inputs
                    var i = $(this); // current input
                    var rule = i.attr('data-rule');
                    if (rule !== undefined) {
                      var ierror = false; // error flag for current input
                      var pos = rule.indexOf(':', 0);
                      if (pos >= 0) {
                        var exp = rule.substr(pos + 1, rule.length);
                        rule = rule.substr(0, pos);
                      } else {
                        rule = rule.substr(pos + 1, rule.length);
                      }
                      switch (rule) {
                        case 'required':
                          if (i.val() === '') {
                            ferror = ierror = true;
                          }
                          break;
                        case 'minlen':
                          if (i.val().length < parseInt(exp)) {
                            ferror = ierror = true;
                          }
                          break;
                        case 'email':
                          if (!emailExp.test(i.val())) {
                            ferror = ierror = true;
                          }
                          break;
                        case 'checked':
                          if (!i.attr('checked')) {
                            ferror = ierror = true;
                          }
                          break;
                        case 'regexp':
                          exp = new RegExp(exp);
                          if (!exp.test(i.val())) {
                            ferror = ierror = true;
                          }
                          break;
                      }
                      i.next('.validation').html(' < i class = \"fa fa-info\"></i> &nbsp;' + ( ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '' )).show();!ierror ? i.next('.validation').hide() : i.next('.validation').show();
                      }
                    });
                  if (ferror) return false;
                  else var str = $(this).serialize(); $.ajax({
                    type: "POST",
                    url: "<?= $base_url ?>/subscribe",
                    data: str,
                    success: function(msg) {
                      if (msg == 'OK') {
                        $("#subscribesendmessage").addClass("show");
                        $("#subscribeerrormessage").removeClass("show");
                        $("#subscribe_name").val('');
                        $("#subscribe_email").val('');
                      } else {
                        $("#subscribesendmessage").removeClass("show");
                        $("#subscribeerrormessage").addClass("show");
                        $('#subscribeerrormessage').html(msg);
                      }
                    }
                  });
                  return false;
                });
            });
    </script>

    <script>

      // Custom
      // var base_url = 'https://dev.bioota.com';
      var base_url = '<?= $base_url ?>';
      var base_url_admin = '<?= $base_url_admin ?>';
      var products = [];
      var product_categories = [];
      var product_images = [];
      var default_data = null;

      $( document ).ready(function() {
        $.when(
          getProfile(), 
          getProduct(), 
          getProductCategory(),
          getProductImage()
        ).done(function(a1, a2) {
          getData()
        })
        // getSettingCaret();
      });

      function getSettingCaret() {
        var toggler = document.getElementsByClassName("myCaret");
        var i;

        for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("myCaret-down");
        });
        }
      }

        function viewProduct(params) {
          let name = params.name;
          
          let html_slides = '';
          let html_slides2 = '';
          let no = 1;
          let default_slide = null;
          $.each(product_images, function(k3, v3) {
            
            if (v3.product_id == params.id) {
              if (default_slide == null) {
                default_slide = v3.id;
              }
              html_slides += `<div class="mySlides" id="product_image_${v3.id}">
                <div class="numbertext">1 / 6</div>
                  <img src="${base_url_admin}/storage/product/${v3.image}" style="width:100%; height: 300px; object-fit: cover;">
              </div>`
              html_slides2 += `<div class="column">
                <img class="demo cursor" src="${base_url_admin}/storage/product/${v3.image}" style="width:100%; height: 50px; object-fit: cover;" onclick="currentSlide(${v3.id}, ${no})" alt="Picture 1">
              </div>`
              // html_slides2 += `<div class="column">
              //   <span class="demo cursor">${no}</span>
              // </div>`;
              no++;
            }
          })

          let detail = '';
          $.each(params.detail.size, function(k, v) {
            detail += `<span>${v}</span><br>`
          })

          let html = `<!-- Full-width images with number text -->
            <h3>${params.name}</h3>

            ${html_slides}

            <!-- Next and previous buttons -->`+
            // <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            // <a class="next" onclick="plusSlides(1)">&#10095;</a>

            `<!-- Image text -->
            <div class="caption-container">
              <p id="caption"></p>
            </div>

            <!-- Thumbnail images -->
            <div class="row">
              <div class="col-lg-12">
                ${html_slides2}
              </div>
            </div>

            <div id="product_detail" class="row">
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <span>Processing Type</span><br>
                <span>Source</span><br>
                <span>Catch Area</span><br>
                <span>Packing</span><br>
                <span>Production Mode</span><br>
                <span>Size</span><br>
              </div>
              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                <span>: ${params.detail.processing_type}</span><br>
                <span>: ${params.detail.source}</span><br>
                <span>: ${params.detail.catch_area}</span><br>
                <span>: ${params.detail.packing}</span><br>
                <span>: ${params.detail.production_mode}</span><br>
                <span>: ${detail}</span><br>
              </div>
            </div>
          `;
          $('#product_view').html(html);
          
          $('.product').removeClass('product_active');
          $(`#product_${params.id}`).addClass('product_active');
          // alert(`#product_${params.id}`)

          showSlides(default_slide, 1);
        }

        // Custom Slider
        let slideIndex = 1;
        showSlides(slideIndex);

        // Next/previous controls
        function plusSlides(n) {
          showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n, no) {
          showSlides(slideIndex = n, no);
        }

        function showSlides(n, no) {
          $(`.mySlides`).css('display', 'none')
          $(`#product_image_${n}`).css('display', 'block')
          $('#caption').text(`Picture ${no}`)
          // let i;
          // let slides = document.getElementsByClassName("mySlides");
          // let dots = document.getElementsByClassName("demo");
          // let captionText = document.getElementById("caption");
          // if (n > slides.length) {slideIndex = 1}
          // if (n < 1) {slideIndex = slides.length}
          // for (i = 0; i < slides.length; i++) {
          //   slides[i].style.display = "none";
          // }
          // for (i = 0; i < dots.length; i++) {
          //   dots[i].className = dots[i].className.replace(" active", "");
          // }
          // slides[slideIndex-1].style.display = "block";
          // dots[slideIndex-1].className += " active";
          // captionText.innerHTML = dots[slideIndex-1].alt;
        }

        function getData() {
          let html = '';
          let default_sidebar = false;
          let default_sidebar_2 = false;

          $.each(product_categories, function(k, v) {
            let html_product = '';

            $.each(products, function(k2, v2) {
              if (v2.category_id == v.id) {
                html_product += `<li class="product${default_sidebar_2 ? '' : ' product_active'}" id="product_${v2.id}" onclick="viewProduct({
                  id: ${v2.id},
                  name: '${v2.name}',
                  detail: {
                    processing_type: '${v2.processing_type}',
                    source: '${v2.source}',
                    catch_area: '${v2.catch_area}',
                    packing: '${v2.packing}',
                    production_mode: '${v2.production_mode}',
                    size: {
                      0: '${v2.size}'
                    }
                  }
                })">
                  ${v2.name} (${v2.alias})
                </li>`
              }

              if (!default_sidebar_2) {
                default_sidebar_2 = true;
              }
              if (default_data == null) {
                default_data = {
                  id: v2.id,
                  name: v2.name,
                  detail: {
                    processing_type: v2.processing_type,
                    source: v2.source,
                    catch_area: v2.catch_area,
                    packing: v2.packing,
                    production_mode: v2.production_mode,
                    size: {
                      0: v2.size
                    }
                  }
                }
              }
            });

            html += `<li>
              <span class="myCaret${default_sidebar ? '' : ' myCaret-down'}"><b>${v.name}</b></span>
              <ul class="nested${default_sidebar ? '' : ' active'}">
                ${html_product}
              </ul>
            </li>`

            if (!default_sidebar) {
              default_sidebar = true;
            }
          });
          
          $('#myUL').html(html);
        
          var toggler = document.getElementsByClassName("myCaret");
          var i;

          for (i = 0; i < toggler.length; i++) {
          toggler[i].addEventListener("click", function() {
              this.parentElement.querySelector(".nested").classList.toggle("active");
              this.classList.toggle("myCaret-down");
          });
          }

          console.log(default_data)
          viewProduct(default_data);
        }
        
        function getProfile() {
          return $.ajax({
            // 'url' : `${base_url}/api/profile`,
            'url' : `https://bioota.com/api/profile`,
            'type' : 'GET',
            'success' : function(res) {
              if (res.success) {
                profile = res.data[0];
                $('.profile_address').html(profile.address)
                $('.profile_address2').html(profile.address2)
                $('.profile_phone').html(`<a href="tel:${profile.phone}">
                  <span dir="ltr">${profile.phone}</span>
                </a>`);
                $('.profile_email').html(`<a href="mailto:${profile.email}">${profile.email}</a>`);
                $('.profile_email').html(profile.email)
              }  
            },
            'error' : function(request,error) {
              alert("Request: "+JSON.stringify(request));
            }
          });
        }

        function getProduct() {
          return $.ajax({
            'url' : `${base_url}/api/product`,
            'type' : 'GET',
            'success' : function(res) {
              if (res.success) {
                products = res.data;
              }  
            },
            'error' : function(request,error) {
              alert("Request: "+JSON.stringify(request));
            }
          });
        }

        function getProductCategory() {
          return $.ajax({
            'url' : `${base_url}/api/product_category`,
            'type' : 'GET',
            'success' : function(res) {
              if (res.success) {
                product_categories = res.data;
              }  
            },
            'error' : function(request,error) {
                alert("Request: "+JSON.stringify(request));
            }
          });
        }

        function getProductImage() {
          return $.ajax({
            'url' : `${base_url}/api/product_image`,
            'type' : 'GET',
            'success' : function(res) {
              if (res.success) {
                product_images = res.data;
              }  
            },
            'error' : function(request,error) {
                alert("Request: "+JSON.stringify(request));
            }
          });
        }

        // Test Push
        // Test Push Again
    </script>
  </body>
</html>