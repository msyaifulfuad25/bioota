<div id="preloader" style="
        height: 100%;
        width: 100%;
    ">
    <div style="
        position: fixed;
        left: 0;
        bottom: 10%;
        z-index: 9999999;
        width: 100%;
        height: 100%;
        overflow: visible;
        background: #fff url('/assets/frontend/img/Preloader.gif') no-repeat center center;
    "></div>
</div>

<script>
    function hidePreloader() {
        $('#preloader').fadeOut();
    }
</script>