<?php

namespace App\Http\Controllers\Custom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ArticleController extends Controller {

    public function detail(Request $request) {
        $id = $request->segments()[1];
        $article = DB::table('custom_article')
            ->orderBy('id', 'ASC')
            ->where('id', $id)
            ->first();

        return view('custom.frontEnd.article.index', compact(
            'article',
        ));
    }

}