<?php

namespace App\Http\Controllers\Custom\APIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ArticleController extends Controller {

    public function get() {
        $article = DB::table('custom_article')
            ->orderBy('id', 'DESC')
            ->limit(3)
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Get article Succesfully',
            'data' => $article
        ]);
    }

    public function find(Request $request) {
        $id = $request->segments()[2];
        $article = DB::table('custom_article')
            ->where('id', $id)
            ->orderBy('id', 'DESC')
            ->first();

        return response()->json([
            'success' => true,
            'message' => 'Find article Succesfully',
            'data' => $article
        ]);
    }

}