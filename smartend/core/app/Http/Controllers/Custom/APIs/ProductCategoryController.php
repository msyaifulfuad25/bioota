<?php

namespace App\Http\Controllers\Custom\APIs;

use App\Http\Controllers\Controller;
use DB;

class ProductCategoryController extends Controller {

    public function get() {
        $product_categories = DB::table('custom_product_category')
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Get Product Category Succesfully',
            'data' => $product_categories
        ]);
    }

}