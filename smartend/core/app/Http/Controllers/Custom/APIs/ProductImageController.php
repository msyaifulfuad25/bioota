<?php

namespace App\Http\Controllers\Custom\APIs;

use App\Http\Controllers\Controller;
use DB;

class ProductImageController extends Controller {

    public function get() {
        $product_images = DB::table('custom_product_image')
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Get Product Image Succesfully',
            'data' => $product_images
        ]);
    }

}