<?php

namespace App\Http\Controllers\Custom\APIs;

use App\Http\Controllers\Controller;
use DB;

class ProductController extends Controller {

    public function get() {
        $product = DB::table('custom_product')
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Get Product Succesfully',
            'data' => $product
        ]);
    }

}