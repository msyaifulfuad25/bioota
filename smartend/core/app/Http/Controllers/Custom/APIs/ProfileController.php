<?php

namespace App\Http\Controllers\Custom\APIs;

use App\Http\Controllers\Controller;
use DB;

class ProfileController extends Controller {

    public function get() {
        $profile = DB::table('custom_profile')
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Get Profile Succesfully',
            'data' => $profile
        ]);
    }

}