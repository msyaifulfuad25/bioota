<?php

namespace App\Http\Controllers\Custom\APIs;

use App\Http\Controllers\Controller;
use DB;

class HomeController extends Controller {

    public function get() {
        $home = DB::table('custom_home')
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Get home Succesfully',
            'data' => $home
        ]);
    }

}