<?php

namespace App\Http\Controllers\Custom;

use App\Http\Controllers\Controller;
use DB;

class HomeFController extends Controller {

    public function index() {
        $home = DB::table('custom_home')
            ->orderBy('id', 'ASC')
            ->get();

        return view('custom.frontEnd.home.index', compact(
            'home',
        ));
    }

}