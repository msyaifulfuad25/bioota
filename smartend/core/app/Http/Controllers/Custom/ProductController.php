<?php

namespace App\Http\Controllers\Custom;

use App\Http\Controllers\Controller;
use DB;

class ProductController extends Controller {

    public function index() {
        $product_categories = DB::table('custom_product_category')
            ->orderBy('id', 'ASC')
            ->get();
        $product_images = DB::table('custom_product_image')
            ->orderBy('id', 'ASC')
            ->get();
        $products = DB::table('custom_product')
            ->orderBy('id', 'ASC')
            ->get();

        return view('custom.frontEnd.product.index', compact(
            'product_categories',
            'product_images',
            'products',
        ));
    }

}